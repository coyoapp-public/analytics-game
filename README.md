## Welcome to the COYO Analytics coding game!

### Preparations
- Checkout this repo.
- Install the [Terraform CLI](https://terraform.io)
- Install [Yarn](https://classic.yarnpkg.com/en/)
- Install [NestJS](https://nestjs.com/)
- Run `cd analytics-adapter && yarn install && yarn start` and check if the application is running
---
## Practical task
Will be explained by the COYO Developer with whom you be will doing this game.

### Theoretical task

```
Please think about how you would implement the COYO Analytics system architecture 
shown below within AWS with a serverless approach with AWS managed services. 
```

![Image](./static/Analytcis_System_architecture.png)
