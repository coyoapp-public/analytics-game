import {Injectable} from '@nestjs/common';
import {EventCreator, TimelinePostCreatedEvent} from './event-creator.service';
import {interval, Observable} from 'rxjs';
import {map, tap} from 'rxjs/operators';

@Injectable()
export class EventClientService {
  constructor(private eventStreamCreator: EventCreator) {
  }

  /**
   * Subscribe here to get events.
   * @return Observable of TimelinePostCreatedEvent
   */
  getEvents$(): Observable<TimelinePostCreatedEvent> {
    return interval(1000).pipe(map(() => this.eventStreamCreator.createRandomEvent()));
  }
}

