import {Injectable} from '@nestjs/common';
import * as faker from 'faker';

/**
 * This service is just for creating mocked events.
 */
@Injectable()
export class EventCreator {

  createRandomEvent(): TimelinePostCreatedEvent {
    return {
      id: faker.random.number(),
      entityId: faker.random.uuid(),
      tenantId: faker.random.uuid(),
      payload: {
        authorId: faker.random.uuid(),
        recipientId: faker.random.uuid(),
        content: faker.lorem.sentences()
      }
    };
  }
}

export interface TimelinePostCreatedEvent {
  id: number;
  entityId: string;
  tenantId: string;
  payload: TimelinePostCreatedPayload;
}

interface TimelinePostCreatedPayload {
  authorId: string;
  recipientId: string;
  content: string;
}
