import { Test, TestingModule } from '@nestjs/testing';
import { EventClientService } from './event-client.service';
import {EventCreator} from './event-creator.service';

describe('EventClientService', () => {
  let service: EventClientService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [EventClientService, EventCreator],
    }).compile();

    service = module.get<EventClientService>(EventClientService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
