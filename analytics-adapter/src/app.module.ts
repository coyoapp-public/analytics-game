import { Module } from '@nestjs/common';
import { EventCreator } from './event-client/event-creator.service';
import { EventClientService } from './event-client/event-client.service';

@Module({
  imports: [],
  providers: [EventCreator, EventClientService],
})
export class AppModule {}
